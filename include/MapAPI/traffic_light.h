/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_TRAFFICLIGHT_H
#define MAPSDK_MAPAPI_TRAFFICLIGHT_H

#include <string>
#include <vector>

#include "Common/common.h"
#include "lane.h"
#include "logical_lane_assignment.h"

namespace map_api
{

struct TrafficLight
{
  struct TrafficSignal
  {
    enum class Color : std::uint8_t
    {
      kUnknown = 0,
      kOther = 1,
      kRed = 2,
      kYellow = 3,
      kGreen = 4,
      kBlue = 5,
      kWhite = 6
    };

    enum class Icon : std::uint8_t
    {
      kUnknown = 0,
      kOther = 1,
      kNone = 2,
      kArrowStraightAhead = 3,
      kArrowLeft = 4,
      kArrowDiagLeft = 5,
      kArrowStraightAheadLeft = 6,
      kArrowRight = 7,
      kArrowDiagRight = 8,
      kArrowStraightAheadRight = 9,
      kArrowLeftRight = 10,
      kArrowDown = 11,
      kArrowDownLeft = 12,
      kArrowDownRight = 13,
      kArrowCross = 14,
      kPedestrian = 15,
      kWalk = 16,
      kDontWalk = 17,
      kBicycle = 18,
      kPedestrianAndBicycle = 19,
      kCountdownSeconds = 20,
      kCountdownPercent = 21,
      kTram = 22,
      kBus = 23,
      kBusAndTram = 24
    };

    enum class Mode : std::uint8_t
    {
      kUnknown = 0,
      kOther = 1,
      kOff = 2,
      kConstant = 3,
      kFlashing = 4,
      kCounting = 5
    };

    Identifier id{UndefinedId};
    BaseProperties base;
    Color color;
    Icon icon;
    Mode mode;
    double counter;
    bool is_out_of_service;

    std::vector<RefWrapper<Lane>> assigned_lanes;
    std::vector<LogicalLaneAssignment> logical_lane_assignments;

    std::string model_reference;
    std::vector<ExternalReference> source_references;
  };

  Identifier id{UndefinedId};
  std::vector<TrafficSignal> traffic_signals;
};

}  // namespace map_api

#endif  // MAPSDK_MAPAPI_TRAFFICLIGHT_H
