/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_TRAFFICSIGNCOMMON_H
#define MAPSDK_MAPAPI_TRAFFICSIGNCOMMON_H

#include <string>

#include "Common/common.h"

namespace map_api
{

struct TrafficSignValue
{
  enum class Unit : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kNoUnit = 2,
    kKilometerPerHour = 3,
    kMilePerHour = 4,
    kMeter = 5,
    kKilometer = 6,
    kFeet = 7,
    kMile = 8,
    kMetricTon = 9,
    kLongTon = 10,
    kShortTon = 11,
    kHour = 15,
    kMinutes = 12,
    kDayOfMonth = 16,
    kDay = 13,
    kPercentage = 14,
    kDurationDay = 17,
    kDurationHour = 18,
    kDurationMinute = 19
  };

  double value;
  Unit value_unit;
  std::string text;
};

enum class TrafficSignVariability : uint8_t
{
  kUnknown = 0,
  kOther = 1,
  kFixed = 2,
  kVariable = 3
};

}  // namespace map_api

#endif  // MAPSDK_MAPAPI_TRAFFICSIGNCOMMON_H
