/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_STATIONARYOBJECT_H
#define MAPSDK_MAPAPI_STATIONARYOBJECT_H

#include <string>
#include <vector>

#include "Common/common.h"
#include "lane.h"
#include "logical_lane_assignment.h"

namespace map_api
{

struct StationaryObject
{
  enum class Type : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kBridge = 2,
    kBuilding = 3,
    kPole = 4,
    kPylon = 5,
    kDelineator = 6,
    kTree = 7,
    kBarrier = 8,
    kVegetation = 9,
    kCurbstone = 10,
    kWall = 11,
    kVerticalStructure = 12,
    kRectangularStructure = 13,
    kOverheadStructure = 14,
    kReflectiveStructure = 15,
    kConstructionSiteElement = 16,
    kSpeedBump = 17,
    kEmittingStructure = 18

  };

  enum class Color : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kYellow = 2,
    kGreen = 3,
    kBlue = 4,
    kViolet = 5,
    kRed = 6,
    kOrange = 7,
    kBlack = 8,
    kGrey = 9,
    kWhite = 10
  };

  enum class Material : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kWood = 2,
    kPlastic = 3,
    kConcrete = 4,
    kMetal = 5,
    kStone = 6,
    kGlas = 7,
    kMud = 8
  };

  Identifier id{UndefinedId};
  BaseProperties base;

  Type type;
  Material material;
  Color color;

  std::vector<RefWrapper<Lane>> assigned_lanes;
  std::vector<double> assigned_lane_percentages;
  std::vector<LogicalLaneAssignment> logical_lane_assignments;

  std::string model_reference;
  std::vector<ExternalReference> source_references;
};

}  // namespace map_api

#endif  // MAPSDK_MAPAPI_STATIONARYOBJECT_H
