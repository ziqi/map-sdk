/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_REFERENCELINE_H
#define MAPSDK_MAPAPI_REFERENCELINE_H

#include <vector>

#include "Common/common.h"

namespace map_api
{
struct ReferenceLinePoint
{
  Position3d world_position;
  units::length::meter_t s_position;
  units::angle::radian_t t_axis_yaw;
};

struct ReferenceLine
{
  enum class Type : std::uint8_t
  {
    TYPE_POLYLINE = 0,
    TYPE_POLYLINE_WITH_T_AXIS = 1
  };

  Identifier id{UndefinedId};
  std::vector<ReferenceLinePoint> poly_line{};
  Type type{Type::TYPE_POLYLINE};
};
}  // namespace map_api

#endif  // MAPSDK_MAPAPI_REFERENCELINE_H
